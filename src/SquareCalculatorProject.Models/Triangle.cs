﻿namespace SquareCalculatorProject.Models
{
    public class Triangle : Figure
    {
        public double B { get; set; }

        public double C { get; private set; }

        public Triangle(double a, double b, double c) : base(a)
        {
            B = b;
            C = c;
        }
    }
}
