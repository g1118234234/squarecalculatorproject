﻿namespace SquareCalculatorProject.Models
{
    public abstract class Figure
    {
        public double A { get; private set; }

        public Figure(double a)
        {
            A = a;
        }
    }
}
