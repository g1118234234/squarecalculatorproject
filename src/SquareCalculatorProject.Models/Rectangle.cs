﻿namespace SquareCalculatorProject.Models
{
    public class Rectangle : Figure
    {
        public double B { get; set; }

        public Rectangle(double a, double b) : base(a)
        {
            B = b;
        }
    }
}
