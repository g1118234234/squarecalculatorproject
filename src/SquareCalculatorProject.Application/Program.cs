﻿using SquareCalculatorProject.Models;
using SquareCalculatorProject.SquareCalculator;


namespace SquareCalculatorProject.Application
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var triangle = new Triangle(3, 3, 3);

            var triangleSquare = Calculator.CalculateSquare(triangle);

            var rectangle = new Rectangle(double.PositiveInfinity, 5);

            var triangle1 = new Triangle(5, 5, -5);

            var rectangleSquare = Calculator.CalculateSquare(rectangle);

            Console.WriteLine($"{triangleSquare:f3}");
            Console.WriteLine($"{rectangleSquare:f3}");
            Console.WriteLine($"{Calculator.CalculateSquare(triangle1)}");
        }
    }
}