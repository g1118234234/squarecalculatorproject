﻿using SquareCalculatorProject.Models;
using SquareCalculatorProject.SquareCalculator;

namespace SquareCalculatorProject.Tests
{
    public class CalculatorTest
    {
        private readonly Random _random;

        public CalculatorTest()
        {
            _random = new Random();
        }

        [Theory]
        [InlineData(3, 3, 3)]
        [InlineData(5, 5, 5)]
        [InlineData(7.5, 7.5, 7.5)]
        [InlineData(0, 0, 0)]
        public void CalculateSquare_WhenValueIsEquilateralTriangle_ShouldReturnTriangleSquare(double a, double b, double c)
        {
            //Arrange
            var triangle = new Triangle(a, b, c);

            //Act
            var expectedResult = Math.Pow(triangle.A, 2) * Math.Sqrt(3) / 4;

            var actualResult = Calculator.CalculateSquare(triangle);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Theory]
        [InlineData(3, 4, 5)]
        public void CalculateSquare_WhenValueIsRectangularTriangle_ShouldReturnTriangleSquare(double a, double b, double c)
        {
            //Arrange
            var triangle = new Triangle(a, b, c);

            double[] sides = new double[3] { triangle.A, triangle.B, triangle.C };

            double[] sortedSides = sides.OrderByDescending(x => x).ToArray();

            //Act
            var expectedResult = 1 / 2 * sortedSides[1] * sortedSides[2];

            var actualResult = Calculator.CalculateSquare(triangle);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CalculateSquare_WhenValueIsRandomTrinagle_ShouldReturnTriangleSquare()
        {
            //Arrange
            var triangle = new Triangle(_random.NextDouble() * 5, _random.NextDouble() * 5, _random.NextDouble() * 5);

            //Act
            var perimeter = triangle.A + triangle.B + triangle.C;

            var expectedResult =  Math.Sqrt(perimeter * (perimeter - triangle.A) + (perimeter - triangle.B) * (perimeter - triangle.C));

            var extualResult = Calculator.CalculateSquare(triangle);

            //Assert
            Assert.Equal(expectedResult, extualResult);
        }

        [Theory]
        [InlineData(2, 2)]
        [InlineData(5.5, 5.5)]
        [InlineData(4, 10)]
        [InlineData(0, 0)]
        public void CalculateSquare_WhenValueIsRectangle_ShouldReturnRectangleSquare(double a, double b)
        {
            //Arrange
            var rectangle = new Rectangle(a, b);

            //Act
            var expectedResult = a * b;

            var actualResult = Calculator.CalculateSquare(rectangle);

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void CalulateSquare_WhenTriangleSideIsLessThanZero_ShouldThrowArithmeticException()
        {
            //Arrange
            var triangle = new Triangle(4, 5, -5);

            //Act
            //Assert
            Assert.Throws<ArithmeticException>(() => Calculator.CalculateSquare(triangle));
        }

        [Fact]
        public void CalculateSquare_WhenRectangleSideIsLessThanZero_ShouldThrowArithmeticException()
        {
            //Arrange
            var rectangle = new Rectangle(5, -7);

            //Act
            //Assert
            Assert.Throws<ArithmeticException>(() => Calculator.CalculateSquare(rectangle));
        }
    }
}
