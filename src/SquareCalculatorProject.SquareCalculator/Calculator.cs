﻿using SquareCalculatorProject.Models;

namespace SquareCalculatorProject.SquareCalculator
{
    public static class Calculator
    {
        public static double CalculateSquare(Figure figure)
        {
            if (figure.GetType() == typeof(Triangle))
            {
                return CalculateTriangleSquare((Triangle)figure);
            }
            else
            {
                return CalculateRectangleSquare((Rectangle)figure);
            }
        }

        private static double CalculateTriangleSquare(Triangle triangle)
        {
            double[] sides = new double[3] { triangle.A, triangle.B, triangle.C };

            double[] sortedSides = sides.OrderByDescending(x => x).ToArray();

            if (triangle.A < 0 || triangle.B < 0 || triangle.C < 0)
            {
                throw new ArithmeticException("Value cannot be less than zero");
            }
            else if (triangle.A == triangle.B && triangle.B == triangle.C)
            {
                return Math.Pow(triangle.A, 2) * Math.Sqrt(3) / 4;
            }
            else if (Math.Pow(sortedSides[0], 2) == (Math.Pow(sortedSides[1], 2) + Math.Pow(sortedSides[2], 2)))
            {
                return 1/2 * sortedSides[1] * sortedSides[2];
            }
            else
            {
                var perimeter = triangle.A + triangle.B + triangle.C;

                return Math.Sqrt(perimeter * (perimeter - triangle.A) + (perimeter - triangle.B) * (perimeter - triangle.C));
            }
        }

        private static double CalculateRectangleSquare(Rectangle rectangle)
        {
            if (rectangle.A < 0 || rectangle.B < 0)
            {
                throw new ArithmeticException("Value cannot be less than zero");
            }

            return rectangle.A * rectangle.B;
        }
    }
}
