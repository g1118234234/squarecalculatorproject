SELECT Products.name, Categories.name
FROM Products
LEFT JOIN ProductCategories 
ON Products.id = ProductCategories.product_id
LEFT JOIN Categories
ON ProductCategories.category_id = Categories.id